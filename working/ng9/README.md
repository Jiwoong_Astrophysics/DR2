# NG9 + PPTA4

This contains the 18 pulsars used in the NG 9 year stochastic paper.

## Specifications

* All data is filtered using a fractional bandwidth of 1.1 (i.e. nu_max/nu_min >= 1.1).
* All JUMPS corresponding to dropped data are removed.
