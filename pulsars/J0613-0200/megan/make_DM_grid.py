import numpy as np

dr2path = '/Users/decesar/Megan/IPTA/DataReleases_GitRepo/DR2'
pulsar = 'J0613-0200'
timfile = 'J0613-0200.tim'
parfile = 'J0613-0200_DR1.par'

junk, tims = np.loadtxt(timfile, unpack=True, dtype='string', comments=("#","FORMAT"))
#print tims

tmin = 1.e20
tmax = 0.0

for i in range(0,len(tims)):
    tim = dr2path+'/pulsars/'+pulsar+'/'+tims[i]
    #print tim
    t = np.loadtxt(tim, usecols=(2,), unpack=True, dtype=float, comments=("FORMAT","TIME","C","MODE","#"))
    for j in range(0,len(t)):
        if t[j] < tmin:
            tmin = t[j]
        if t[j] > tmax:
            tmax = t[j]

print tmin, tmax
print tmax-tmin
print (tmax-tmin)/60.



tmin = '50931.7'
tmax = '56795.6'
tmin=float(int(float(tmin)))
tmax=float(int(float(tmax)+1.))

dmgrid = np.linspace(tmin,tmax,num=50)
print dmgrid

newpar = open(pulsar+'_DMmodel.par', 'w')
par = open(parfile, 'r')
lines = par.readlines()
par.close()

for i in range(0,len(lines)):
    if lines[i].startswith('DM '):
        newpar.write('FD1 0 0\n')
        newpar.write('FD2 0 0\n')
        newpar.write('FD3 0 0\n')
        newpar.write('FD4 0 0\n')
        newpar.write('FD5 0 0\n')
        newpar.write('FD6 0 0\n')
        newpar.write(lines[i])
        newpar.write('DMMODEL DM 1\n')
        for j in range(0,len(dmgrid)):
            newpar.write('DMOFF %.2f 0 0\n'%dmgrid[j])
        newpar.write('CONSTRAIN DMMODEL\n')

    elif (lines[i].startswith('DM')) and (not lines[i].startswith('DM ')):
        continue

    elif lines[i].startswith('UNITS'):
        newpar.write('UNITS   TDB\n')

    else:
        newpar.write(lines[i])

newpar.close()

