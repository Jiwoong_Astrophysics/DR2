#! /usr/bin/env python

# Resets DMX all DMX ranges using a new timespan, then
# separate out any "single-band" epochs.  Will also process all 
# includes, resulting in a single output tim file.  Outputs new par
# and tim files with 'dmxNN' in the names.

import sys
import re
import tempo_utils

try:
    parfile = sys.argv[1]
    timfile = sys.argv[2]
except IndexError:
    print "usage: %s parfile timfile" % (sys.argv[0],)
    exit(1)

# Set true to keep pre-set DMX ranges
keep_old_ranges = True
mjdcut = 55620.0 # Max GASP epoch in 9y set

dmx_t = 6

# Return the value of the (first occurence of) the given key 
# in a set of parfile lines
def parvalue(lines,key,f=str):
    for l in lines:
        stuff = l.split()
        if stuff[0]==key: 
            if f==float:
                return f(stuff[1].replace('D','e'))
            else:
                return f(stuff[1])
    return None

# Return the MJD range of the given DMX bin 
def dmx_range(lines,idx):
    r1 = parvalue(lines,'DMXR1_%04d'%idx,f=float)
    r2 = parvalue(lines,'DMXR2_%04d'%idx,f=float)
    return (r1,r2)

# Return the freq range of the given DMX bin 
def dmx_freq_range(lines,idx):
    f1 = parvalue(lines,'DMXF1_%04d'%idx,f=float)
    f2 = parvalue(lines,'DMXF2_%04d'%idx,f=float)
    return (f1,f2)

parlines = open(parfile).readlines()
toas = tempo_utils.read_toa_file(timfile,
        process_emax=False, process_skips=True)

# Strip out EMAXs, etc
toas = tempo_utils.toalist([t for t in toas if not t.format=='Comment'])
toas = tempo_utils.toalist([t for t in toas if not t.command=='EMAX'])
toas = tempo_utils.toalist([t for t in toas if not t.command=='FORMAT'])
toas = tempo_utils.toalist([t for t in toas if not t.command=='MODE'])

# Time-sort
toas = tempo_utils.toalist(sorted(toas,key=lambda t: t.mjd))

# Put MODE and FORMAT back in
toas.insert(0,tempo_utils.toa("MODE 1"))
toas.insert(0,tempo_utils.toa("FORMAT 1"))

ndmx = sum([l.startswith('DMX_') for l in parlines])

# If DMX_0001 is 'extra-long', keep it.
# This should be fine for nanograv data set.
(r1,r2) = dmx_range(parlines,1)
dmx0001_lines = []
keep_dmx0001 = False
if r1 is not None:
    if (r2-r1) > 100.0:
        dmx0001 = re.compile('DMX.*_0001')
        dmx0001_lines = [l for l in parlines if dmx0001.match(l)]
        keep_dmx0001 = True

# Strip all DMX
if not keep_old_ranges:
    parlines = [l for l in parlines if not l.startswith('DMX')]
else:
    # reset the 'DMX' line itself
    parlines = [l for l in parlines if not l.startswith('DMX ')]
    # Filter out any past the cutoff date
    for idmx in range(1,ndmx+1):
        (r1,r2) = dmx_range(parlines,idmx)
        if r2<mjdcut: continue
        print "Removing DMX_%04d mjd=%.3f" % (idmx, r2)
        dmxre = re.compile('DMX.*_%04d'%idmx)
        parlines = [l for l in parlines if not dmxre.match(l)]

# Add in new DMX settings
parlines += ['DMX %.1f\n' % dmx_t,]
if keep_dmx0001 and not keep_old_ranges:  parlines += dmx0001_lines

# Reset solarn0 while we're at it
parlines = [l for l in parlines if not l.startswith('SOLARN0')]
parlines += ["SOLARN0 0.0\n",]

# Call tempo to generate new bins
(chi2,ndof,rms,newparlines) = tempo_utils.run_tempo(toas,parlines,
        get_output_par=True)

# Check DMX bins for single-band epochs
ndmx = sum([l.startswith('DMX_') for l in newparlines])
bad_dmx = []
for i in range(1,ndmx+1):
    (f1,f2) = dmx_freq_range(newparlines,i)
    if i==1 and keep_dmx0001: continue
    if f2/f1 < 1.1: bad_dmx += [dmx_range(newparlines,i),]

print "%10s: %d/%d bad dmx" % (parvalue(parlines,'PSR'), len(bad_dmx), ndmx)

# Filter out any in bad ranges
removed_toas = tempo_utils.toalist([])
for t in toas:
    if t.is_toa():
        for r in bad_dmx:
            if t.mjd > r[0] and t.mjd < r[1]:
                removed_toas.append(t)
    if t.is_commented_toa():
        tt = tempo_utils.toa(t.line.strip('C# '))
        for r in bad_dmx:
            if tt.mjd > r[0] and tt.mjd < r[1]:
                removed_toas.append(t)

for t in removed_toas: 
    toas.remove(t)

# Write the updated TOA files
toas.write(timfile.replace('.tim','.dmx%d.tim' % dmx_t))
removed_toas.write(timfile.replace('.tim','.dmxcut.tim'))

# Re-run tempo once again to get final DMX bins
(chi2,ndof,rms,dmxparlines) = tempo_utils.run_tempo(toas,parlines,
        get_output_par=True)

open(parfile.replace('.par','.dmx%d.par' % dmx_t),'w').writelines(dmxparlines)
