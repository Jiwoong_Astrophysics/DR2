import numpy as np
from matplotlib import pyplot as plt

mjd, dmoff, dmofferr = np.loadtxt('DMoff.dat', unpack=True, usecols=(1,2,3), dtype='float')

plt.errorbar(mjd, dmoff, yerr = dmofferr, linestyle='none')
plt.xlabel('MJD')
plt.ylabel('DM Offset') # [Fixed DM (at MJD 55000) = 13.297]')
plt.savefig('dmoffset_B1855')
plt.show()
