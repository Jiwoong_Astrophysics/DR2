# simple_auto_merge.sh

* This has been superseded by the python version simple_auto_merge.sh. Please run "simple_auto_merge.py -h" for help. *

Old instructions:

The simple_auto_merge.sh is intended to automatically create a preliminary par and time file for combing IPTA data.

A simple usage case, without creating a DM grid:

```
mkdir -p ~/work/IPTA/
cd ~/work/IPTA/
git clone git@gitlab.com:IPTA/DR2.git
mkdir -p ~/work/IPTA/DR2/test/J1713+0747
cd ~/work/IPTA/DR2/test/J1713+0747
~/work/IPTA/DR2/utils/simple_auto_merge.sh ~/work/IPTA/DR2/ J1713+0747 4
```
If you want to add a DM grid with spacing of 50 days spanning epochs from 52000 to 56850.

```
~/work/IPTA/DR2/utils/simple_auto_merge.sh ~/work/IPTA/DR2/ J1713+0747 4 52000 56850 50
```

Regardless of the mode and pulsar, the script will produce files with the same names: `ipta.par` and `ipta.tim` so make sure not to overwrite your previous work.

Both examples will produce the par and tim file in `~/work/IPTA/DR2/test/J1713+0747`. You can use any subdirectory of the repository.
