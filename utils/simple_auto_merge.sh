#!/bin/bash

set -o nounset
set -o errexit

if [[ $# -ne 3 &&  $# -ne 6 ]] || [ "$1" == "-h" ]
then
  echo usage: $0 REPO_TOP_PATH PSR FD_count PSR_PARAM_UPDATE [FIRST_DMEPOCH LAST_DMEPOCH EPOCH_SPAN]
  echo e.g.: $0 ~/work/IPTA/DR2/ J2145-0750 2
  echo will produce ipta.par and ipta.tim
  echo for J2145-0750 including 2 FD params
  echo
  echo $0 ~/work/IPTA/DR2/ J2145-0750 2 53275.0 56586.1 150
  echo will produce ipta.par and ipta.tim
  echo for J2145-0750 including 2 FD params
  echo and a DM grid with 150 days resolution
  exit
fi

top_path=$1

PSR=$2
FD_count=$3
DM_grid_start=-1
DM_grid_end=-1
DM_grid_step=-1
if [ $# -eq 6 ]
then
  DM_grid_start=$4
  DM_grid_end=$5
  DM_grid_step=$6
fi

count=$(echo $(pwd) | sed -e 's|'$top_path'||' -e 's|^/||'  | awk -F/ '{print NF}')
rel_path=""
for x in $(seq 1 $count)
do
  rel_path="../"$rel_path
done

#
# Add EPTA:
echo "FORMAT 1" > ipta.tim
echo "# EPTA:" >> ipta.tim
for EPTAtim in ${top_path}/EPTA_v2.2/${PSR}/tims/*tim
do
  echo "INCLUDE ${rel_path}/EPTA_v2.2/${PSR}/tims/$(basename $EPTAtim)" >> ipta.tim
done

cat ${top_path}/EPTA_v2.2/${PSR}/${PSR}.par| grep -v -e "^DM1" -e "^DM2" -e "^JUMP" | column -t > ipta.par
echo "UNITS TCB" >> ipta.par

# FD params:
for i in $(seq 1 $FD_count)
do
  echo "FD$i       0.0 1" >> ipta.par
done

echo "# EPTA's JUMPs:" >> ipta.par
cat ${top_path}/EPTA_v2.2/${PSR}/tims/*tim | grep -e '-sys' | awk '{for (i=1;i<=NF; i++) {if ($i ~/-sys/) print "JUMP -sys",$(i+1),"0.0",1}}' | sort | uniq >> ipta.par

# Add NANOGrav 
echo "# NANOGRav:" >> ipta.tim
echo "INCLUDE ${rel_path}NANOGrav_9y/tim/${PSR}_NANOGrav_9yv1.tim" >> ipta.tim
echo "# NANOGrav's JUMPs:" >> ipta.par
cat ${top_path}/NANOGrav_9y/tim/${PSR}_NANOGrav_9yv1.tim | grep -e '-fe' | awk '{for (i=1;i<=NF; i++) {if ($i ~/-fe/) print "JUMP -fe",$(i+1),"0.0",1}}'  | sort | uniq >> ipta.par

# Add PPTA:
echo "# PPTA:" >> ipta.tim
echo "INCLUDE ${rel_path}PPTA_dr1dr2/tim/v2/${PSR}_dr1dr2.tim" >> ipta.tim

echo "# PPTA's JUMPs:" >> ipta.par
cat ${top_path}/PPTA_dr1dr2/tim/v2/${PSR}_dr1dr2.tim | grep -e '-q' | awk '{for (i=1;i<=NF; i++) {if ($i == "-q") {print "JUMP -q",$(i+1),"0.0",1}}}'  | sort | uniq >> ipta.par

echo "# PPTA's fixed JUMPs:" >> ipta.par
cat ${top_path}/utils/PPTA_fixed_JUMPs.list >> ipta.par

# Set one band as a reference:
max_length=0
chosen_flag=""
for EPTAtim in ${top_path}/EPTA_v2.2/${PSR}/tims/*tim
do

  length=$(wc -l $EPTAtim | awk '{print $1}')
  if [ $length -gt $max_length ]
  then
    chosen_flag=$(basename $EPTAtim .tim)
    max_length=$length
  fi
done
# sed -i 's/JUMP -sys '$chosen_flag' 0.0 1/# REFERENCE JUMP:\nJUMP -sys'$chosen_flag' 0.0 0/' ipta.par # doesn't work on OS X, need GNU sed
sed -i "" 's/JUMP -sys '$chosen_flag' 0.0 1/JUMP -sys '$chosen_flag' 0.0 0/' ipta.par 

# DM grid model:
if [ $# -eq 6 ]
then
  echo "# DM grid model" >> ipta.par
  echo "DMMODEL DM 1" >> ipta.par
  current_epoch=$DM_grid_start
  next_epoch=$(echo ${current_epoch}'+'$DM_grid_step | bc -l)
  while [ $(echo ${next_epoch}'<'${DM_grid_end} | bc -l) -eq 1 ]
  do
    echo "DMOFF $current_epoch 0 0" >> ipta.par
    current_epoch=$next_epoch
    next_epoch=$(echo ${current_epoch}'+'$DM_grid_step | bc -l)
  done
  echo "DMOFF $DM_grid_end 0 0" >> ipta.par
  echo "CONSTRAIN DMMODEL" >> ipta.par
fi
