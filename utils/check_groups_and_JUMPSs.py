import numpy as np
import tempo_utils as tu
import sys, os

pta_from_site = {'1': "NANOGrav",
                 'gbt': "NANOGrav",
                 '3': "NANOGrav",
                 'ao': "NANOGrav",
                 'aoutc': "NANOGrav",
                 '8': "EPTA",
                 'ebb': "EPTA",
                 'f': "EPTA",
                 'jb': "EPTA",
                 'g': "EPTA",
                 'w': "EPTA",
                 'ncy': "EPTA",
                 'i': "EPTA",
                 'wsrt': "EPTA",
                 'z': "EPTA",
                 '7': "PPTA",
                 'pks': "PPTA"}

jumps = {'EPTA': {"sys": ["EFF.EBPP.1360",
                          "EFF.EBPP.1410",
                          "EFF.EBPP.2639",
                          "JBO.DFB.1400",
                          "JBO.DFB.1520",
                          "JBO.DFB.5000",
                          "NRT.BON.1400",
                          "NRT.BON.1600",
                          "NRT.BON.2000",
                          "NRT.DDS.1400",
                          "WSRT.P1.1380",
                          "WSRT.P1.1380.1",
                          "WSRT.P1.1380.2",
                          "WSRT.P1.1380.2.C",
                          "WSRT.P1.1380.C",
                          "WSRT.P1.2273.C",
                          "WSRT.P1.323.C",
                          "WSRT.P1.328",
                          "WSRT.P1.328.C",
                          "WSRT.P1.367.C",
                          "WSRT.P1.382",
                          "WSRT.P1.382.C",
                          "WSRT.P1.840",
                          "WSRT.P1.840.C"]},
         'PPTA': {"g": ["10CM_PDFB1",
                        "10CM_PDFB2",
                        "10CM_PDFB4",
                        "10CM_WBCORR",
                        "20CM_PDFB2",
                        "20CM_PDFB3",
                        "20CM_PDFB4",
                        "20CM_WBCORR",
                        "40CM_PDFB3",
                        "50CM_CPSR2",
                        "40CM_PDFB3"],
                  "h": ["20CM_H-OH_CPSR2m",
                        "20CM_H-OH_CPSR2n",
                        "20CM_H-OH_PDFB1",
                        "20CM_MULTI_CPSR2m",
                        "20CM_MULTI_CPSR2n",
                        "20CM_MULTI_PDFB1"]},
         'NANOGrav': {"fe": ["430",
                             "L-wide",
                             "S-wide",
                             "Rcvr_800",
                             "Rcvr1_2"],
                      "be": ["mk2_14",
                             "mk2_23",
                             "mk3_14m",
                             "mk3_14r",
                             "mk3_23m"],
                      "f": ["L-band_ABPP",
                            "S-band_ABPP",
                            "L-band_Mark3a",
                            "L-band_Mark3b",
                            "L-band_Mark4",
                            "S-band_Mark4"]}}

jumps_fixed = ["PPTA_fixed_JUMPs.list"]

def process_known_jumps(jumpfile):
    """
    Return a dictionary that has 
    """
    
    return

if __name__=="__main__":

    if len(sys.argv) != 2:
        print "usage:  python check_groups_and_JUMPSs.py TIMFILE"
        sys.exit(0)

    # Get all the groups in all the .tim files (it reads the includes)
    ts = tu.toalist(tu.read_toa_file(sys.argv[1]))

    # Get those that are real TOAs
    toas = [t for t in ts if t.is_toa()]
    N = len(toas)
    print "\nThere are %d TOAs in total." % N
    
    # Check that all the TOAs have a "group" flag set
    try:
        toa_groups = np.unique(ts.get_flag('group'))
    except:
        print "Error:  Not all TOAs have '-group' flag set!"
    
    # keys are tuple of PTA, flag, and setting for JUMPs, vals are TOA errors
    jumpvals = {}
    toas_accounted_for = []
    
    # Now walk through the TOAs and figure out which JUMP groups are present
    for ii, t in enumerate(toas):
        pta = pta_from_site[t.site.lower()]
        for flag in jumps[pta].keys():
            fval = t.flag(flag)
            if fval in jumps[pta][flag]:
                vals = (pta, flag, fval)
                toas_accounted_for.append(ii)
                if vals in jumpvals:
                    jumpvals[vals].append(t.error)
                else:
                    jumpvals[vals] = [t.error]

    # Make sure that all the TOAs are in JUMPs
    if N != len(toas_accounted_for):
        missing_toas = set(range(N)) - set(toas_accounted_for)
        print "There are %d TOAs that are not in JUMPs:\n" % len(missing_toas)
        for ii in missing_toas:
            print toas[ii]

    # Determine which group of TOAs will *not* be JUMPed
    # It is the one with the largest weight in the fit
    # as determined by the sum of their 1/err^2
    wgts = np.zeros(len(jumpvals))
    keys = sorted(jumpvals.keys())
    ns = [len(jumpvals[k]) for k in keys]
    for ii, k in enumerate(keys):
        wgts[ii] = (1.0/np.asarray(jumpvals[k])**2.0).sum()
    best_group_arg = np.argmax(wgts)
    print "The TOA group with the most weight is:", keys[best_group_arg]

    # Print JUMP lines that should go in the parfile
    print "\nThe JUMP lines that should go in the file are:"
    print "-"*46
    for ii, k in enumerate(keys):
        print "JUMP -%-3s %-20s 0  %d" % (k[1], k[2], 0 if ii==best_group_arg else 1)

    # If the PPTA fixed JUMPs need to go in, add them here (TODO)
