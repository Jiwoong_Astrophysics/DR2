import os
import sys
import glob


if len(sys.argv) != 2:
    print "usage: python remove_timfile_leadingspace.py <path_to_tims_folder>"
    sys.exit(0)

alltim = glob.glob('%s/*tim' % sys.argv[1])

for i in range(0,len(alltim)):

    timfile = open(alltim[i], 'r')
    spaces = 0
    lines = timfile.readlines()
    for line in lines:
        if line.startswith(' '):
            spaces = 1

    if spaces == 1:
        os.system('mv %s %s_prev' % (alltim[i], alltim[i]))
        out = open(alltim[i], 'w')
        for line in lines:
            if line.startswith(' '):
                line = line.strip(' ')
            out.write(line)
        out.close()

    spaces = 0
    
