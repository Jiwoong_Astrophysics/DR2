import numpy as np
from collections import deque

def id_toa_line(line):
    """ Determine if the line from a .tim file corresponds to an 
        (uncommented) TOA.
        
        Primarily, check if the third column is consistent with an MJD.
    """
    if line[0]=='#' or line[0]=='C':
        return False
    if 'FORMAT' in line:
        return False
    if 'MODE' in line:
        return False
    toks = line.split()
    if len(toks) < 3:
        return False
    mjd = toks[2].split('.')
    if len(mjd)==2 and len(mjd[0])==5:
        return True
    return False

class TOA(object):

    def __init__(self,line):
        self.line = line
        toks = line.strip().split()
        self.profile = toks[0]
        self.freq = float(toks[1])
        self.mjd = float(toks[2])
        self.err = float(toks[3])
        self.flags = dict()
        for i in xrange((len(toks)-5)/2):
            self.flags[toks[5+i*2]] = toks[5+i*2+1]

def parse_toas(timfile):
    return map(TOA,filter(id_toa_line,file(timfile).readlines()))

def uni_grid(timfile,parfile,output=None,cadence=50,dmerr_thresh=1):
    """ Add a uniform DMX grid to an ephemeris.
    
    timfile -- set of TOAs used to construct grid
    parfile -- ephemeris to which DMX grid will be added
    cadence -- grid spacing (days)
    output -- optional output; if None, will clobber parfile
    dmerr_thresh -- threshold on formal DM uncertainty for free entry

    The algorithm will examine the set of TOAs and use it to build a
    uniform DMX grid with the requested cadence.  Entries are initialized
    to 0.  Using the TOA uncertainties and frequencies, the algorithm
    constructs a formal uncertainty on DMX by inverting the hessian for a
    log likelihood with two free parameters, an unknown overall delay and
    the differential delay between the available frequencies.  If the formal
    uncertainty is above dmerr_thresh, the entry will be fixed to 0.

    NB that this is only an approximation to the true uncertainty, since
    jumps and white noise models will influence the true precision.  Some
    tweaking may be required.

    Additionally, if there are no fixed intervals, DM should not be fit for
    independently.  No check is made for this.
    """
    toas = sorted(parse_toas(timfile),key=lambda t: t.mjd)
    mjds,errs,freqs = np.asarray(
        [(t.mjd,t.err,t.freq) for t in toas]).transpose()
    # entries in the hessian matrix for uncertainty calculation
    dmconsts2 = (4.14e-3/((freqs*1e-3)**2*(errs*1e-6)))**2
    dmconsts1 = 4.14e-3/((freqs*1e-3)*(errs*1e-6))**2
    dmconsts0 = 1./(errs*1e-6)**2
    dmconsts = np.asarray([dmconsts2,dmconsts1,dmconsts0]).transpose()
    grid_lt = np.arange(int(min(mjds)),int(max(mjds))+1,cadence)
    s = deque([''])
    for i in xrange(len(grid_lt)):
        lt,rt = grid_lt[i],grid_lt[i] + cadence
        mask = (mjds >= lt) & (mjds < rt)
        a,b,c = dmconsts[mask,:].sum(axis=0)
        # 0th element of inverse hessian
        dmerr = (c/(a*c-b*b))**0.5
        s.append('DMX_%04d 0 %d'%(i+1,dmerr<dmerr_thresh))
        s.append('DMXR1_%04d %.5f'%(i+1,lt))
        s.append('DMXR2_%04d %.5f'%(i+1,rt))
    s.append('')
    lines = filter(lambda x: 'DMX' not in x, file(parfile).readlines())
    lines.append('\n'.join(s))
    file(output or parfile,'w').write(''.join(lines))

if __name__ == '__main__':

    import argparse

    desc = 'Insert a uniform DMX grid into an ephemeris.'
    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument('parfile',nargs=1,help='Specify the ephemeris to which the grid will be added.')
    parser.add_argument('timfile',nargs=1,help='Specify the TOA file that will be used to generate grid.')
    parser.add_argument('--output',default=None,help='Optional output ephemeris.  If not specified, parfile argument will be clobbered.')
    parser.add_argument('--cadence',type=float,default=50,help='Specify the spacing for DMX intervals (days).')
    parser.add_argument('--dmerr_thresh',type=float,default=0.1,help='Specify the minimum DM uncertainty allowed.')

    args = parser.parse_args()

    print 'Inserting a grid of %d days using TOAs from %s.'%(args.cadence,args.timfile[0])

    uni_grid(args.timfile[0],args.parfile[0],output=args.output,
        cadence=args.cadence,dmerr_thresh=args.dmerr_thresh)

    print 'Output written to %s.'%(args.output or args.parfile[0])
