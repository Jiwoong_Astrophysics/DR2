import pint.toa as toa
import numpy as np
import matplotlib.pyplot as plt
import astropy.units as u
import os.path
import os, glob, sys

def mjd_to_year(MJD):
    return (MJD - 47892.0) / 365.25 + 1990.0 

cwd = os.getcwd()
DR2path = '../'

PTAs = ['EPTA', 'PPTA', 'NANOGrav']
obss = ['wsrt', 'parkes', 'jodrell', 'gbt', 'effelsberg', 'arecibo', 'nancay', 'ncyobs']
scopes = {'EPTA':['wsrt', 'jodrell', 'effelsberg', 'nancay', 'ncyobs'], 
          'PPTA':['parkes'], 'NANOGrav':['gbt', 'arecibo']}

PTA_colors = {'EPTA':'g', 'PPTA':'r', 'NANOGrav': 'b'}

topdir = os.path.join(DR2path, "release")
os.chdir(topdir)
psrs = glob.glob("J*")
TOAs = {}
minMJDs = []
maxMJDs = []

for psr in psrs:
    print psr
    os.chdir(os.path.join(topdir, psr))
    TOAs[psr] = toa.TOAs(psr+".IPTADR2v1.tim")
    locobss = TOAs[psr].get_obss()
    for obs in locobss:
        if obs not in obss:
            print "'%s' not in obss!"
            sys.exit()
    minMJDs.append([TOAs[psr].first_MJD.value, psr])
    maxMJDs.append([TOAs[psr].last_MJD.value, psr])
os.chdir(cwd)

# PSRs are sorted by min MJD
minMJDs = sorted(minMJDs)
minMJD = min([x for x, y in minMJDs])
minMJD = 49990.0
maxMJD = max([x for x, y in maxMJDs])
# Space to hold the PSR names
extra_days = (maxMJD-minMJD) * 0.15
lox, hix = minMJD - 0.95 * extra_days, maxMJD + 0.05 * extra_days
Npsrs = len(minMJDs)

# The following is a plot that shows the PSRs as a function of PTA

fig1 = plt.figure(figsize=(8.5, 11), facecolor='white')
ax1 = plt.axes(frameon=False)
ax1.axes.get_yaxis().set_visible(False)
ax1.set_ylim(0.0, 1.0)
ax1.set_xlim(lox, hix)
ax1.set_xlabel("MJD")
dy = 1.0 / (Npsrs + 3)
y = 0.5 * dy
for date, psr in minMJDs:
    ts = TOAs[psr]
    for PTA in PTAs:
        if PTA=='PPTA': exy = -0.2 * dy
        if PTA=='EPTA': exy =  0.2 * dy
        if PTA=='NANOGrav': exy = 0.0
        for scope in scopes[PTA]:
            try:
                ts.select(ts.get_obss() == scope)
                mjds = np.unique(ts.get_mjds().value.astype(np.int))
                mjds = mjds[mjds > minMJD] 
                print psr, PTA, scope, len(mjds)
                if len(mjds):
                       plt.plot(mjds, np.ones(len(mjds)) * y + exy,
                                PTA_colors[PTA]+'.', label=PTA, markersize=3)
            except IndexError:
                pass
            ts.unselect()
    plt.text(lox+0.01*(hix-lox), y, psr, verticalalignment='center', fontsize=8)
    y += dy
y += dy
for dx, PTA in zip([1.0/6, 0.5, 1.0-1.0/6], PTAs):
    plt.text(lox+dx*(hix-lox), y, PTA, color=PTA_colors[PTA], fontsize=12,
             horizontalalignment='center', verticalalignment='center')
ax2 = ax1.twiny() 
ax2.axes.get_yaxis().set_visible(False)
ax2.set_ylim(0.0, 1.0)
ax2.set_xlim(mjd_to_year(lox), mjd_to_year(hix))
ax2.set_xlabel("Year")
#plt.show()
plt.savefig("IPTA_DR2_by_PTAs.png")

# The following is a plot that shows the TOAs as functions of observing freq

freq_names = ['low', 'mid', 'Lband', 'Sband']
freq_ranges = {'low':[1.0, 500.0], 'mid':[500.0, 1000.0],
               'Lband':[1000.0, 2000.0], 'Sband':[2000.0, 10000.0]}
freq_colors = {'low': 'red', 'mid': 'orange',
               'Lband': 'green', 'Sband': 'blue'}
freq_labels = {'low':'< 0.5 GHz', 'mid':'0.5-1 GHz',
               'Lband':"1-2 GHz", 'Sband':'> 2 GHz'}
freq_exys = {'low': -0.2, 'mid': -0.07,
             'Lband': +0.07, 'Sband': +0.2}
fig1 = plt.figure(figsize=(8.5, 11), facecolor='white')
ax1 = plt.axes(frameon=False)
ax1.axes.get_yaxis().set_visible(False)
ax1.set_ylim(0.0, 1.0)
ax1.set_xlim(lox, hix)
ax1.set_xlabel("MJD")
dy = 1.0 / (Npsrs + 3)
y = 0.5 * dy
for date, psr in minMJDs:
    ts = TOAs[psr]
    for name in freq_names:
        try:
            ts.select(np.logical_and(ts.get_freqs() > freq_ranges[name][0] * u.MHz,
                                     ts.get_freqs() <= freq_ranges[name][1] * u.MHz))
            mjds = np.unique(ts.get_mjds().value.astype(np.int))
            mjds = mjds[mjds > minMJD] 
            print psr, len(mjds)
            if len(mjds):
                plt.plot(mjds, np.ones(len(mjds)) * y + freq_exys[name] * dy, '.',
                         c=freq_colors[name], label=freq_labels[name], markersize=3)
        except IndexError:
            pass
        ts.unselect()
    plt.text(lox+0.01*(hix-lox), y, psr, verticalalignment='center', fontsize=8)
    y += dy
y += dy
for dx, name in zip([1.0/8, 3.0/8, 5.0/8, 7.0/8], freq_names):
    plt.text(lox+dx*(hix-lox), y, freq_labels[name], color=freq_colors[name], fontsize=12,
             horizontalalignment='center', verticalalignment='center')

ax2 = ax1.twiny() 
ax2.axes.get_yaxis().set_visible(False)
ax2.set_ylim(0.0, 1.0)
ax2.set_xlim(mjd_to_year(lox), mjd_to_year(hix))
ax2.set_xlabel("Year")
#plt.show()
plt.savefig("IPTA_DR2_by_freqs.png")
