# IPTA DR2

This is the repository for the data combination group for the
International Pulsar Timing Array's 2nd Data Release (DR2).

The combination team consists of the following people:

Scott Ransom <sransom@nrao.edu>  (Project Coordinator)

**EPTA:**
  * Ben Perera <bhakthiperera@gmail.com>
  * Lindley Lentati <ltl21@mrao.cam.ac.uk>
  * Lucas Guillemot <lucas.guillemot@cnrs-orleans.fr> (Telescope expert)
  * Gemma Janssen <janssen@astron.nl> (Telescope expert)
  * David Champion <davidjohnchampion@gmail.com> (Telescope expert)
  * Marta Burgay <burgay@ca.astro.it> (Telescope expert)

**NANOGrav:**
  * Megan DeCesar <megandecesar@gmail.com>
  * Paul Demorest <pdemores@nrao.edu> (Telescope expert)
  * David Nice <niced@lafayette.edu> (Telescope expert)

**PPTA:**
  * Matthew Kerr <matthew.kerr@gmail.com>
  * Stefan Oslowski <stefan.oslowski@gmail.com>
